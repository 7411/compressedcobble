minetest.register_node("compressedcobble:compressedcobble", {
	description = "Indubitably compressed cobblestone",
	tiles = {"compressedcobble.png"},
	stack_max = 1337,
	groups = {cracky = 3, stone = 1}
})

minetest.register_craft({
	output = "compressedcobble:compressedcobble",
	recipe = {
		{"default:cobble", "default:cobble", "default:cobble"},
		{"default:cobble", "default:cobble", "default:cobble"},
		{"default:cobble", "default:cobble", "default:cobble"}
	}
})

minetest.register_craft({
	type = "shapeless",
	output = "default:cobble 9",
	recipe = {"compressedcobble:compressedcobble"}
})
